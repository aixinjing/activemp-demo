package com.qlk.activemqdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.Topic;
import java.util.stream.IntStream;

@RestController
public class Proudcer {
    private final static Logger log= LoggerFactory.getLogger(Proudcer .class);
    @Autowired
    private JmsTemplate jmsTemplate;
    @Autowired
    private Queue queue;
    @Autowired
    private Topic topic;

    @RequestMapping("send")
    private String sendMessage() {
        try {
            String  name=queue.getQueueName();
           /* TbItem tbItem = new TbItem();
            tbItem.setId(1L);
            tbItem.setImage("image");*/
            IntStream.rangeClosed(0,5).forEach(num->{
                jmsTemplate.send(name, session -> session.createObjectMessage(new Person(num,0,"aa")));
            });
        } catch (JMSException e) {
            e.printStackTrace();
        }
        return "success";
    }

}
