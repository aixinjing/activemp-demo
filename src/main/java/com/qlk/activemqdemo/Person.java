package com.qlk.activemqdemo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Auther:艾新静
 * @Date: 2018/7/19 11:06
 * @Description:
 */
@Data
@AllArgsConstructor
@ToString
public class Person implements Serializable{
    private Integer id;
    private Integer Count;
    private String name;
}
