package com.qlk.activemqdemo;

import org.apache.activemq.command.ActiveMQObjectMessage;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class ItemESMessageListener {


    @JmsListener(destination="queue",containerFactory = "jmsListener")
    public void onMessage(Message message, Session session) throws JMSException {
        ActiveMQObjectMessage objectMessage=(ActiveMQObjectMessage)message;
        Person person = (Person) objectMessage.getObject();
        try {
            System.out.println("time:"+new SimpleDateFormat("HH:mm:ss").format(new Date())+"消息:"+person+"重试次数："+objectMessage.getRedeliveryCounter());
            if(person.getId()==3){
                Integer i=null;
                i.toString();
            }
            message.acknowledge();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            person.setCount(person.getCount()+1);
            if(objectMessage.getRedeliveryCounter()==4){
               // message.setJMSDeliveryTime(1*1000L);
            }
            if(objectMessage.getRedeliveryCounter()==5){
                System.out.println("发送到数据库记录下吧");
            }
            session.recover();
        }}}